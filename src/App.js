import React, { useState, useEffect, useCallback } from "react";
import Carousel from "react-slick";
import "./App.css";

const images = ["1", "2", "3", "4", "5", "6"];

function App() {
  const [slider1, setSlider1] = useState(null);
  const [slider2, setSlider2] = useState(null);

  const settingsImages = {
    dots: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: false,
  };

  const settingsThumbnail = {
    dots: false,
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
  };

  return (
    <div>
      <h1>First Carousel</h1>
      <Carousel
        asNavFor={slider2}
        ref={(slider) => setSlider1(slider)}
        {...settingsImages}
      >
        {images.map((image, index) => {
          return (
            <div>
              <p key={`image-${index}`}>{image}</p>
            </div>
          );
        })}
      </Carousel>
      <h1>Second Carousel</h1>
      <Carousel
        asNavFor={slider1}
        ref={(slider) => setSlider2(slider)}
        swipeToSlide={true}
        focusOnSelect={true}
        {...settingsThumbnail}
      >
        {images.map((image, index) => {
          return (
            <div>
              <p key={`thumbnail-${index}`}>{image}</p>
            </div>
          );
        })}
      </Carousel>
    </div>
  );
}

export default App;
